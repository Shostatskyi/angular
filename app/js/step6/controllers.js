'use strict';

bookApp.controller('BookListCtrl', function($scope, $http) {
    $http.get('data/books.json').success(function(data) {
        $scope.books = data;
    });
	$scope.orderProperty = 'name';
	$scope.orderProperty = 'price';
	$scope.orderProperty = 'genre';
	$scope.orderProperty = 'pages';
	$scope.filterProperty = 'genre';

});

bookApp.controller('BookSingleCtrl', function($scope, $http) {
    $http.get('data/book.json').success(function(data) {
        $scope.book = data;
    });
});

